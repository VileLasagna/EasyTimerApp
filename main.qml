import QtQuick 2.10
import QtQuick.Window 2.10
import QtGraphicalEffects 1.0

Window {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Easy Timer")
    color: colorBG


    property double time: 3000
    property double timerangle: 0
    property string colorA : timerangle < 360 ? "#FFFF0000" : "#FF00FF00"
    property string colorB : timerangle > 360 ? "#FFFF0000" : "#FF00FF00"
    property string colorBG : "#FF000000"
    property double glowRadius: 0
    property int maxGlowRadius: 60


    PropertyAnimation on timerangle
    {
        id: timerAnimation
        loops: Animation.Infinite
        from: 0
        to: 720
        duration: time*2
    }

    MouseArea
    {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked:{
            if(mouse.button == Qt.RightButton)
            {
                timerAnimation.restart();
                timerAnimation.pause();
            }
            else
            {
                if (timerAnimation.paused)
                {

                    timerAnimation.resume();
                }
                else
                {
                    timerAnimation.pause();
                }
            }
        }
    }

    SequentialAnimation
    {
        id: glowPulse

        NumberAnimation {
            target: mainWindow
            property: "glowRadius"
            duration: 100
            easing.type: Easing.OutBack
            from: 0
            to: maxGlowRadius
        }

        NumberAnimation {
            target: mainWindow
            property: "glowRadius"
            duration: 300
            easing.type: Easing.InCubic
            from: maxGlowRadius
            to: 0
        }

    }

    Rectangle
    {
        id: mainTimer
        anchors.centerIn: parent
        height: 300
        width: 300
        radius: 300
        color: colorA

        ConicalGradient
        {
            angle: 0
            anchors.fill: parent
            gradient: Gradient {
                               GradientStop { position: 0.00; color: colorB }
                               GradientStop { position: (timerangle%360)/360; color: colorB }
                               GradientStop { position: ((timerangle%360)/360) + 0.002; color: "transparent" }
                               GradientStop { position: 1.00; color: "transparent" }
                               }
            source: parent

        }
    }

    Rectangle
    {
        id: oneSecMarker
        anchors.bottom: mainTimer.verticalCenter
        anchors.left: mainTimer.horizontalCenter
        height: 4
        width: mainTimer.width * 0.6
        color: colorBG
        Rectangle
        {
            id: oneSecTip
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            width: mainTimer.width * 0.1
            height: parent.height
            color: (timerangle%360) > 120 ? colorB : colorA
            onColorChanged: glowPulse.start()
        }
        transform: Rotation { origin.x: 0; origin.y: 1.5; angle: 30}
    }

    Rectangle
    {
        id: twoSecMarker
        anchors.bottom: mainTimer.verticalCenter
        anchors.left: mainTimer.horizontalCenter
        height: 4
        width: mainTimer.width * 0.6
        color: colorBG
        Rectangle
        {
            id: twoSecTip
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            width: mainTimer.width * 0.1
            height: parent.height
            color: (timerangle%360) > 240 ? colorB : colorA
            onColorChanged: glowPulse.start()
        }
        transform: Rotation { origin.x: 0; origin.y: 1.5; angle: 150}
    }

    Rectangle
    {
        id: threeSecMarker
        anchors.bottom: mainTimer.verticalCenter
        anchors.left: mainTimer.horizontalCenter
        height: 4
        width: mainTimer.width * 0.6
        color: colorBG
        Rectangle
        {
            id: threeSecTip
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            width: mainTimer.width * 0.1
            height: parent.height
            color: colorB
            onColorChanged: glowPulse.start()
        }
        transform: Rotation { origin.x: 0; origin.y: 1.5; angle: 270}
    }

    Glow
    {
        z: -10
        anchors.fill: mainTimer
        radius: glowRadius
        samples: 1+glowRadius*2
        color: colorA
        source: mainTimer
        transparentBorder: true
        spread: 0.3
    }
}
